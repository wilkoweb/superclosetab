
# <img src="icon.png" width="45" align="left"> Super Close Tab 1.1.13

> Chrome extension that adds a simple close button navigation to browser pages, for touch screen kiosk panels / tills

- Close button with dialog box to make sure want to close
- Error page shown that allows a retry or close page
- Error page showing URL and error message
- Up and Down buttons to scroll up and down
- Back button
- Remove cookies when start browser
- Works with older chrome versions in case being used on 32-bit till or kiosk
- ASM click to close session if required (and pause)

## Install

Currently only installation through the Chrome Development mode is allowed. You can easily pack the extension and install it normally though!


## Created by

- [Simon Huggins](https://github.com/huggyfee)


## License

MIT © [Simon Huggins](http://www.simonhuggins.com)
