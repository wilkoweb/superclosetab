chrome.runtime.onMessage.addListener(function(message,sender,sendResponse) {
  if(message.goto_url){
    setTimeout ( function () {
		chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
		  chrome.tabs.remove(tabs[0].id)
		  sendResponse({"status": "ok"});
		});
	  }, message.pauseValue || 200);
  } else if(message.errorpage){
      chrome.tabs.remove(message.errorpage, function() { });
  }
});

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    checkCloseTab(tabId, changeInfo, tab);
});

chrome.tabs.onCreated.addListener(function(tabId, changeInfo, tab) {
   checkCloseTab(tabId, changeInfo, tab);
});

function removeAllCookies(domains) {
  for (var d=0; d<domains.length; d++) {
    chrome.cookies.getAll({domain: domains[d]}, function(cookies) {
      for(var i=0; i<cookies.length;i++) {
        var cookieUrl = "http" + (cookies[i].secure ? "s" : "") + "://" + cookies[i].domain + cookies[i].path;
        var cookieName = cookies[i].name;
        chrome.cookies.remove({url: cookieUrl, name: cookieName});
      }
    });
  }
};

function checkCloseTab(tabId, changeInfo, tab)
{
	if (typeof changeInfo !== 'undefined' && typeof changeInfo.url !== 'undefined' && changeInfo.url.length > 0)
	{
		if (changeInfo.url.toLowerCase().indexOf('closechrometab') != -1)
		{
		    removeAllCookies(["beta.wilko.com","uat.wilko.com","oat.wilko.com","asm.wilko.com","dev.wilko.com"]);
			chrome.tabs.remove(tabId);
		}
	}
}

chrome.webNavigation.onErrorOccurred.addListener(function (details) {

    var urlStart = details.url.substring(0, details.url.indexOf('.com/') + ".com".length);

    if(urlStart === "https://asm.wilko.com"){
      var whitelistedErrors = ['net::ERR_ABORTED','net::ERR_INSECURE_RESPONSE','net::ERR_CONNECTION_TIMED_OUT'];
    } else { 
      var whitelistedErrors = ['No whitelisted errors!'];
    }
    if(whitelistedErrors.indexOf(details.error) < 0){
      var encodedErrorInfo =  encodeURIComponent("<b>Error:</b> "+details.error.slice(0,30));
      chrome.tabs.update(details.tabId, {
      url: chrome.extension.getURL("page/errorpage.html?errorInfo="+encodedErrorInfo)
      });
    }
});
