if(document.URL.startsWith("chrome-extension://")) {

	$(document).on('click',"#superclosetab-closeTab", event => {
		chrome.tabs.getCurrent(function(tab) {
    		var currentTab = tab.id;
    		chrome.runtime.sendMessage({errorpage: tab.id});
		});
	});

	$(document).on('click','#superclosetab-tryAgain', event => {
	  window.history.back();
	});

	function findGetParameter(parameterName) {
		var result = null,
			tmp = [];
		location.search
			.substr(1)
			.split("&")
			.forEach(function (item) {
			  tmp = item.split("=");
			  if (tmp[0] === parameterName) result = tmp[1];
			});
		return result;
	}

	function urldecode(str) {
	   return decodeURIComponent((str+'').replace(/\+/g, '%20'));
	}

	$(document).ready(function() {
		  var encodedErrorInfo = findGetParameter("errorInfo");
		  var decodedErrorInfo = urldecode(encodedErrorInfo);
		  $(".errormsg").html(decodedErrorInfo);
	});

}
