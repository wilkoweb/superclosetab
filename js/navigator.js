'use strict';

function setupNavigator() {

  const injectedControls = '<div id="superclosetab-navigator"><button id="superclosetab-backbutton" class="superclosetab-button"><button id="superclosetab-downbutton" class="superclosetab-button" class="superclosetab-button"><button id="superclosetab-upbutton" class="superclosetab-button"><button id="superclosetab-closebutton" class="superclosetab-button"></button><div class="superclosetab-closeconfirm"><hr /><h1 class="superclosetab-closeconf-txt">Have you finished?</h1><button class="superclosetab-buttonfw superclosetab-closeyes">Yes</button</div><button class="superclosetab-buttonfw superclosetab-closeno">No</button</div></div>';
  $('body').prepend(injectedControls);
  $('#superclosetab-backbutton').css('background-image', 'url(' + chrome.extension.getURL('/img/backbtn.png') + ')');
  $('#superclosetab-upbutton').css('background-image', 'url(' + chrome.extension.getURL('/img/upbtn.png') + ')');
  $('#superclosetab-downbutton').css('background-image', 'url(' + chrome.extension.getURL('/img/downbtn.png') + ')');
  $('#superclosetab-closebutton').css('background-image', 'url(' + chrome.extension.getURL('/img/closebtn.png') + ')');
}
$(document).on('click', '#superclosetab-closebutton', event => {
  $('.superclosetab-closeconfirm').slideDown();
});
$(document).on('click', '.superclosetab-closeno', event => {
  $('.superclosetab-closeconfirm').slideUp();
  });

function sendCloseChromeMessage(pauseValueInMs) {
	chrome.runtime.sendMessage({goto_url: "http://closechrometab", pauseValue: pauseValueInMs});
}

function hidePDPVideo(){
  if($('.pdp-image-gallery__item-video').length > 0){
    if($('.pdp-image-gallery__item-video').parent().parent().data('slickIndex')){
      var slideIndex = $('.pdp-image-gallery__item-video').parent().parent().data('slickIndex');
      $('.pdp-image-gallery [data-slick-index="'+ slideIndex +'"]').hide();
    }
  }
}

$(document).on('click', '.superclosetab-closeyes', event => {
	if(document.getElementById("stopEmulate") != null) {
	  // Found ASM 'close session' button so clicking it and waiting a bit (1.5s)
		$("#stopEmulate").click();
    	sendCloseChromeMessage(1500);
	} else {
    sendCloseChromeMessage(0);
  }
});

$(document).on('click', '#superclosetab-upbutton', event => {
	window.scrollBy(0, -300)
});

$(document).on('click', '#superclosetab-downbutton', event => {
	window.scrollBy(0, 300);
});

$(document).on('click', '#superclosetab-backbutton', event => {
	window.history.back();
});


$(function () {
  if(!document.URL.startsWith("chrome-extension://")) {
    setupNavigator();
    hidePDPVideo();
  }
});
